﻿namespace PmjCtl.UI.WinForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lbPrinted = new System.Windows.Forms.ListBox();
            this.btnUpLoad = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbAuto = new System.Windows.Forms.CheckBox();
            this.lbUnPrint = new System.Windows.Forms.ListBox();
            this.numCount = new System.Windows.Forms.NumericUpDown();
            this.btnSet = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtPrint = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnGetSN = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnVerify = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtOrder = new System.Windows.Forms.TextBox();
            this.txtItem = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnContinue = new System.Windows.Forms.Button();
            this.btnDisconnet = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.numPort = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lstClient = new System.Windows.Forms.ListBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txtContent = new System.Windows.Forms.TextBox();
            this.btnSetContent = new System.Windows.Forms.Button();
            this.btnClearCache = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.numCount2 = new System.Windows.Forms.NumericUpDown();
            this.btnSetPrintCount = new System.Windows.Forms.Button();
            this.btnOpenDevice = new System.Windows.Forms.Button();
            this.btnCancelPrint = new System.Windows.Forms.Button();
            this.lstMsg = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnSaveConfig = new System.Windows.Forms.Button();
            this.txtUrl1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bsUnPrint = new System.Windows.Forms.BindingSource(this.components);
            this.bsPrinted = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCount)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCount2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsUnPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsPrinted)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1183, 678);
            this.tabControl1.TabIndex = 43;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1175, 649);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "主页面";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lbPrinted);
            this.groupBox2.Controls.Add(this.btnUpLoad);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(714, 51);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(430, 420);
            this.groupBox2.TabIndex = 47;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "流程三";
            // 
            // lbPrinted
            // 
            this.lbPrinted.FormattingEnabled = true;
            this.lbPrinted.ItemHeight = 25;
            this.lbPrinted.Location = new System.Drawing.Point(179, 51);
            this.lbPrinted.Name = "lbPrinted";
            this.lbPrinted.Size = new System.Drawing.Size(204, 254);
            this.lbPrinted.TabIndex = 54;
            // 
            // btnUpLoad
            // 
            this.btnUpLoad.Location = new System.Drawing.Point(220, 311);
            this.btnUpLoad.Name = "btnUpLoad";
            this.btnUpLoad.Size = new System.Drawing.Size(135, 39);
            this.btnUpLoad.TabIndex = 53;
            this.btnUpLoad.Text = "上传结果";
            this.btnUpLoad.UseVisualStyleBackColor = true;
            this.btnUpLoad.Click += new System.EventHandler(this.btnUpLoad_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label10.Location = new System.Drawing.Point(24, 51);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(148, 27);
            this.label10.TabIndex = 52;
            this.label10.Text = "确认OK条码";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbAuto);
            this.groupBox3.Controls.Add(this.lbUnPrint);
            this.groupBox3.Controls.Add(this.numCount);
            this.groupBox3.Controls.Add(this.btnSet);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.txtPrint);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btnGetSN);
            this.groupBox3.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(37, 208);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(634, 403);
            this.groupBox3.TabIndex = 46;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "流程二";
            // 
            // cbAuto
            // 
            this.cbAuto.AutoSize = true;
            this.cbAuto.Location = new System.Drawing.Point(436, 145);
            this.cbAuto.Name = "cbAuto";
            this.cbAuto.Size = new System.Drawing.Size(184, 29);
            this.cbAuto.TabIndex = 54;
            this.cbAuto.Text = "自动设定内容";
            this.cbAuto.UseVisualStyleBackColor = true;
            // 
            // lbUnPrint
            // 
            this.lbUnPrint.FormattingEnabled = true;
            this.lbUnPrint.ItemHeight = 25;
            this.lbUnPrint.Location = new System.Drawing.Point(163, 145);
            this.lbUnPrint.Name = "lbUnPrint";
            this.lbUnPrint.Size = new System.Drawing.Size(217, 229);
            this.lbUnPrint.TabIndex = 53;
            // 
            // numCount
            // 
            this.numCount.Location = new System.Drawing.Point(163, 47);
            this.numCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numCount.Name = "numCount";
            this.numCount.Size = new System.Drawing.Size(217, 36);
            this.numCount.TabIndex = 52;
            this.numCount.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(436, 101);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(135, 34);
            this.btnSet.TabIndex = 51;
            this.btnSet.Text = "设定内容";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(36, 103);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 27);
            this.label1.TabIndex = 50;
            this.label1.Text = "正在打印";
            // 
            // txtPrint
            // 
            this.txtPrint.Location = new System.Drawing.Point(163, 101);
            this.txtPrint.Name = "txtPrint";
            this.txtPrint.Size = new System.Drawing.Size(217, 36);
            this.txtPrint.TabIndex = 49;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(9, 145);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 27);
            this.label5.TabIndex = 48;
            this.label5.Text = "待打印条码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(36, 49);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 27);
            this.label2.TabIndex = 46;
            this.label2.Text = "申请个数";
            // 
            // btnGetSN
            // 
            this.btnGetSN.Location = new System.Drawing.Point(436, 47);
            this.btnGetSN.Name = "btnGetSN";
            this.btnGetSN.Size = new System.Drawing.Size(135, 34);
            this.btnGetSN.TabIndex = 44;
            this.btnGetSN.Text = "获取SN号";
            this.btnGetSN.UseVisualStyleBackColor = true;
            this.btnGetSN.Click += new System.EventHandler(this.btnGetSN_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnVerify);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtOrder);
            this.groupBox1.Controls.Add(this.txtItem);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(37, 32);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(634, 149);
            this.groupBox1.TabIndex = 45;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "流程一";
            // 
            // btnVerify
            // 
            this.btnVerify.Location = new System.Drawing.Point(464, 36);
            this.btnVerify.Name = "btnVerify";
            this.btnVerify.Size = new System.Drawing.Size(107, 78);
            this.btnVerify.TabIndex = 39;
            this.btnVerify.Text = "验证";
            this.btnVerify.UseVisualStyleBackColor = true;
            this.btnVerify.Click += new System.EventHandler(this.btnVerify_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(8, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 27);
            this.label3.TabIndex = 37;
            this.label3.Text = "工单号";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(8, 84);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 27);
            this.label4.TabIndex = 38;
            this.label4.Text = "产品编码";
            // 
            // txtOrder
            // 
            this.txtOrder.Location = new System.Drawing.Point(135, 36);
            this.txtOrder.Name = "txtOrder";
            this.txtOrder.Size = new System.Drawing.Size(310, 36);
            this.txtOrder.TabIndex = 1;
            // 
            // txtItem
            // 
            this.txtItem.Location = new System.Drawing.Point(135, 78);
            this.txtItem.Name = "txtItem";
            this.txtItem.Size = new System.Drawing.Size(310, 36);
            this.txtItem.TabIndex = 1;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnContinue);
            this.tabPage2.Controls.Add(this.btnDisconnet);
            this.tabPage2.Controls.Add(this.btnSave);
            this.tabPage2.Controls.Add(this.numPort);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.lstClient);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.btnOpenDevice);
            this.tabPage2.Controls.Add(this.btnCancelPrint);
            this.tabPage2.Controls.Add(this.lstMsg);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1175, 649);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "喷码机控制";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnContinue
            // 
            this.btnContinue.Location = new System.Drawing.Point(547, 453);
            this.btnContinue.Margin = new System.Windows.Forms.Padding(4);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(100, 29);
            this.btnContinue.TabIndex = 72;
            this.btnContinue.Text = "继续喷印";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // btnDisconnet
            // 
            this.btnDisconnet.Location = new System.Drawing.Point(655, 416);
            this.btnDisconnet.Margin = new System.Windows.Forms.Padding(4);
            this.btnDisconnet.Name = "btnDisconnet";
            this.btnDisconnet.Size = new System.Drawing.Size(100, 29);
            this.btnDisconnet.TabIndex = 71;
            this.btnDisconnet.Text = "断开连接";
            this.btnDisconnet.UseVisualStyleBackColor = true;
            this.btnDisconnet.Click += new System.EventHandler(this.btnDisconnect_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(484, 525);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 70;
            this.btnSave.Text = "保存";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // numPort
            // 
            this.numPort.Location = new System.Drawing.Point(336, 523);
            this.numPort.Margin = new System.Windows.Forms.Padding(4);
            this.numPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numPort.Name = "numPort";
            this.numPort.Size = new System.Drawing.Size(97, 25);
            this.numPort.TabIndex = 69;
            this.numPort.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label9.Location = new System.Drawing.Point(253, 525);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 15);
            this.label9.TabIndex = 68;
            this.label9.Text = "服务端口:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 298);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 15);
            this.label6.TabIndex = 67;
            this.label6.Text = "连接的喷码机";
            // 
            // lstClient
            // 
            this.lstClient.FormattingEnabled = true;
            this.lstClient.ItemHeight = 15;
            this.lstClient.Location = new System.Drawing.Point(36, 322);
            this.lstClient.Name = "lstClient";
            this.lstClient.Size = new System.Drawing.Size(156, 169);
            this.lstClient.TabIndex = 66;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txtContent);
            this.groupBox5.Controls.Add(this.btnSetContent);
            this.groupBox5.Controls.Add(this.btnClearCache);
            this.groupBox5.Location = new System.Drawing.Point(239, 391);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(277, 100);
            this.groupBox5.TabIndex = 65;
            this.groupBox5.TabStop = false;
            // 
            // txtContent
            // 
            this.txtContent.Location = new System.Drawing.Point(151, 29);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(120, 25);
            this.txtContent.TabIndex = 64;
            // 
            // btnSetContent
            // 
            this.btnSetContent.Location = new System.Drawing.Point(17, 25);
            this.btnSetContent.Margin = new System.Windows.Forms.Padding(4);
            this.btnSetContent.Name = "btnSetContent";
            this.btnSetContent.Size = new System.Drawing.Size(116, 29);
            this.btnSetContent.TabIndex = 56;
            this.btnSetContent.Text = "设定内容";
            this.btnSetContent.UseVisualStyleBackColor = true;
            this.btnSetContent.Click += new System.EventHandler(this.btnSendContent_Click);
            // 
            // btnClearCache
            // 
            this.btnClearCache.Location = new System.Drawing.Point(17, 62);
            this.btnClearCache.Margin = new System.Windows.Forms.Padding(4);
            this.btnClearCache.Name = "btnClearCache";
            this.btnClearCache.Size = new System.Drawing.Size(116, 29);
            this.btnClearCache.TabIndex = 46;
            this.btnClearCache.Text = "清除缓存";
            this.btnClearCache.UseVisualStyleBackColor = true;
            this.btnClearCache.Click += new System.EventHandler(this.btnClearCache_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.numCount2);
            this.groupBox4.Controls.Add(this.btnSetPrintCount);
            this.groupBox4.Location = new System.Drawing.Point(239, 310);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(277, 66);
            this.groupBox4.TabIndex = 64;
            this.groupBox4.TabStop = false;
            // 
            // numCount2
            // 
            this.numCount2.Location = new System.Drawing.Point(151, 25);
            this.numCount2.Name = "numCount2";
            this.numCount2.Size = new System.Drawing.Size(120, 25);
            this.numCount2.TabIndex = 64;
            this.numCount2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnSetPrintCount
            // 
            this.btnSetPrintCount.Location = new System.Drawing.Point(17, 25);
            this.btnSetPrintCount.Margin = new System.Windows.Forms.Padding(4);
            this.btnSetPrintCount.Name = "btnSetPrintCount";
            this.btnSetPrintCount.Size = new System.Drawing.Size(116, 29);
            this.btnSetPrintCount.TabIndex = 53;
            this.btnSetPrintCount.Text = "设置喷印次数";
            this.btnSetPrintCount.UseVisualStyleBackColor = true;
            this.btnSetPrintCount.Click += new System.EventHandler(this.btnSetPrintCount_Click);
            // 
            // btnOpenDevice
            // 
            this.btnOpenDevice.Enabled = false;
            this.btnOpenDevice.Location = new System.Drawing.Point(655, 453);
            this.btnOpenDevice.Margin = new System.Windows.Forms.Padding(4);
            this.btnOpenDevice.Name = "btnOpenDevice";
            this.btnOpenDevice.Size = new System.Drawing.Size(100, 29);
            this.btnOpenDevice.TabIndex = 51;
            this.btnOpenDevice.Text = "查询状态";
            this.btnOpenDevice.UseVisualStyleBackColor = true;
            // 
            // btnCancelPrint
            // 
            this.btnCancelPrint.Location = new System.Drawing.Point(547, 416);
            this.btnCancelPrint.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelPrint.Name = "btnCancelPrint";
            this.btnCancelPrint.Size = new System.Drawing.Size(100, 29);
            this.btnCancelPrint.TabIndex = 62;
            this.btnCancelPrint.Text = "取消喷印";
            this.btnCancelPrint.UseVisualStyleBackColor = true;
            this.btnCancelPrint.Click += new System.EventHandler(this.btnCancelPrint_Click);
            // 
            // lstMsg
            // 
            this.lstMsg.FormattingEnabled = true;
            this.lstMsg.ItemHeight = 15;
            this.lstMsg.Location = new System.Drawing.Point(36, 16);
            this.lstMsg.Margin = new System.Windows.Forms.Padding(4);
            this.lstMsg.Name = "lstMsg";
            this.lstMsg.Size = new System.Drawing.Size(1063, 274);
            this.lstMsg.TabIndex = 45;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnSaveConfig);
            this.tabPage3.Controls.Add(this.txtUrl1);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1175, 649);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "MES接口";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnSaveConfig
            // 
            this.btnSaveConfig.Location = new System.Drawing.Point(554, 220);
            this.btnSaveConfig.Name = "btnSaveConfig";
            this.btnSaveConfig.Size = new System.Drawing.Size(75, 23);
            this.btnSaveConfig.TabIndex = 8;
            this.btnSaveConfig.Text = "保存";
            this.btnSaveConfig.UseVisualStyleBackColor = true;
            this.btnSaveConfig.Click += new System.EventHandler(this.btnSaveConfig_Click);
            // 
            // txtUrl1
            // 
            this.txtUrl1.Location = new System.Drawing.Point(137, 66);
            this.txtUrl1.Name = "txtUrl1";
            this.txtUrl1.Size = new System.Drawing.Size(504, 25);
            this.txtUrl1.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(51, 76);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "接口一地址";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(48, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "接口列表";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1218, 721);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "喷码机内容控制";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCount)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numCount2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bsUnPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsPrinted)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtOrder;
        private System.Windows.Forms.TextBox txtItem;
        private System.Windows.Forms.ListBox lstMsg;
        private System.Windows.Forms.Button btnVerify;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnGetSN;
        private System.Windows.Forms.Button btnClearCache;
        private System.Windows.Forms.Button btnSetContent;
        private System.Windows.Forms.Button btnOpenDevice;
        private System.Windows.Forms.Button btnSetPrintCount;
        private System.Windows.Forms.Button btnCancelPrint;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox lstClient;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txtContent;
        private System.Windows.Forms.NumericUpDown numCount2;
        private System.Windows.Forms.NumericUpDown numPort;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtPrint;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnUpLoad;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtUrl1;
        private System.Windows.Forms.Button btnSaveConfig;
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.NumericUpDown numCount;
        private System.Windows.Forms.BindingSource bsUnPrint;
        private System.Windows.Forms.BindingSource bsPrinted;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ListBox lbPrinted;
        private System.Windows.Forms.ListBox lbUnPrint;
        private System.Windows.Forms.Button btnDisconnet;
        private System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.CheckBox cbAuto;
    }
}

