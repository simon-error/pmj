﻿using PMJ.Instruct;
using PMJ.Socket;
using PMJ.Socket.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace PmjCtl.UI.WinForm
{
    public partial class Form1 : Form
    {
        private List<string> unPrintSN;
        private List<string> printedSN;
        bool bAutoSetFlag;
        bool IsVerify = false;
        bool IsEmpty = false;
        public Form1()
        {
            InitializeComponent();
            unPrintSN = new List<string>();
            printedSN = new List<string>();
            bsUnPrint.DataSource = unPrintSN;
            bsPrinted.DataSource = printedSN;
            bsUnPrint.ListChanged += BsUnPrint_ListChanged;
            lbUnPrint.DataSource = bsUnPrint;
            lbPrinted.DataSource = bsPrinted;
            bAutoSetFlag = true;

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            txtUrl1.Text = ConfigurationManager.AppSettings["action1"];
            numPort.Value = int.Parse( ConfigurationManager.AppSettings["sport"]);
            cbAuto.Checked = bool.Parse(ConfigurationManager.AppSettings["auto"]);

            SocketConnectionServerDispatcher.ServerSocketListing(new SocketConnectConfig()
            {
                KeepAliveTime = 2 * 60 * 1000,//心跳间隔2分钟
                AcceptCallback = con =>
                {
#if DEBUG
                    lstMsg.Invoke(new Action(() =>
                    {
                        lstMsg.Items.Add($"有连接到达{con.ConnectSocket.RemoteEndPoint}");
                    }));
#endif
                    lstClient.Invoke(new Action(() =>
                    {
                        lstClient.Items.Add(con.Identity);
                        SocketConnectionServerDispatcher.DicSockectConnection.AddOrUpdate(con.Identity, con, (key, old) => con);
                    }));
                },
                ReceiveCallback = (bytes, con) =>
                {
#if DEBUG
                    lstMsg.Invoke(new Action(() =>
                    {
                        lstMsg.Items.Add($"接收消息{string.Join(" ",bytes.Select(c=>$"{c:X2}"))}");
                    }));
#endif
                    Process(bytes, con);
                },
                DisConnectCallback = con =>
                {
                    SocketConnection currSocket;
                    if (con.Identity == null || !SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(con.Identity, out currSocket)) return;
                    if (con != currSocket) return;
                    SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(con.Identity, out currSocket);
                    lstMsg.Invoke(new Action(() =>
                    {
                        lstClient.Items.Remove(con.Identity);
#if DEBUG
                        lstMsg.Items.Add($"有连接断开{con.ConnectSocket.RemoteEndPoint}-----{con.Identity}");
#endif
                    }));
                },
                ShowMsg = msg =>
                {
#if DEBUG
                    lstMsg.Invoke(new Action(() =>
                    {
                        lstMsg.Items.Add(msg);
                    }));
#endif
                },
                SocketConnectExceptionCallback = ex =>
                {
#if DEBUG
                    lstMsg.Invoke(new Action(() =>
                    {
                        lstMsg.Items.Add(ex.Message);
                    }));
#endif
                },
                BeforeSend = (bs, con) =>
                {
#if DEBUG
                    lstMsg.Invoke(new Action(() =>
                    {
                        lstMsg.Items.Add($"耗时:{DateTime.Now - con.LastSessionTime}发送命令{string.Join(" ", bs.Select(c => $"{c:X2}"))}");
                    }));
#endif
                }
            });
        }

        private void Process(byte[] bytes, SocketConnection con)
        {
            byte[] body = bytes.Skip(1).ToArray();
            if (body[0] == 0x44)//登入
            {
                string node = Encoding.UTF8.GetString(bytes, 2, 6);
                if (node == "Myname")
                {
                    //存在粘包问题
                    //con.Identity = Encoding.UTF8.GetString(bytes, 8, bytes.Length - 11);
                    var msg = Encoding.ASCII.GetString(bytes);
                    int pos = msg.IndexOf("\r\n");
                    if (pos < 0) 
                        return;

                    //con.Identity = Encoding.UTF8.GetString(bytes, 8, pos - 9);
                    SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(con.Identity, out con);
                    con.Identity = Encoding.UTF8.GetString(bytes, 8, pos - 9)+(con.ConnectSocket.RemoteEndPoint as IPEndPoint).Address.ToString();
                    lstClient.Invoke(new Action(() =>
                    {
                        lstClient.Items.Remove((con.ConnectSocket.RemoteEndPoint as IPEndPoint).Address.ToString());
                        lstClient.Items.Add(con.Identity);
                        SocketConnectionServerDispatcher.DicSockectConnection.AddOrUpdate(con.Identity, con, (key, old) => con);
                    }));
                    btnClearCache_Click(null,null);
                    SetCount(con,"1");
                }
            }
            else if (Encoding.UTF8.GetString(body, 0, 3) == "OUT")//退出
            {
                byte[] cmd = PmjInstruct.JointCmd(PmjCmd.DisConnect, null);
                con.Send(cmd);
            }
            else if (Encoding.UTF8.GetString(body, 0, 3) == "OK5") //缓存打印完毕
            {
                //printedSN.Add(txtPrint.Text);
                //printedSN = printedSN.Distinct().ToList();
                if (bAutoSetFlag)
                {
                    this.Invoke(new Action(() =>
                    {
                        //bsPrinted.ResetBindings(false);
                        if (txtPrint.Text!=string.Empty)
                        {
                            bsPrinted.Add(txtPrint.Text);
                            btnUpLoad_Click(null, null);
                        }

                        if (bsUnPrint.List?.Count != 0)
                        {
                            txtPrint.Text = bsUnPrint.List?[0].ToString();
                            bsUnPrint.Remove(txtPrint.Text);
                        }
                        else
                        {
                            this.IsEmpty = true;
                        }
                    }));
                    if (txtPrint.Text != string.Empty)
                    {

                    byte[] cmd = PmjInstruct.JointCmd(PmjCmd.SetContent, txtPrint.Text);
                    con.Send(cmd);
                    }
                }
            }
        }

        private void btnSendContent_Click(object sender, EventArgs e)
        {
            if (txtContent.Text == "")
            {
                MessageBox.Show("请输入设定内容");
                return;
            }
            var bs = PMJ.Instruct.PmjInstruct.JointCmd(PmjCmd.SetContent, txtContent.Text.ToString());
            if (lstClient.Items.Count == 0)
            {
                MessageBox.Show("没有连接的客户端");
                return;
            }
            List<string> selectList = new List<string>();
            foreach (var selectedItem in lstClient.Items)
            {
                selectList.Add(selectedItem.ToString());
            }
            SocketConnection socketcon;
            foreach (var select in selectList)
            {
                if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(select, out socketcon) &&
                socketcon.ConnectSocket.Connected)
                {
                    socketcon.Send(bs);
                }
                else
                {
                    lstClient.Items.Remove(select);
                    SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(select, out socketcon);
                }
            }
        }

        private void btnVerify_Click(object sender, EventArgs e)
        {
            StringBuilder soap = new StringBuilder();
            soap.Append("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\r\n");
            soap.Append("  <soap12:Body>\r\n");
            soap.Append("    <LaserCarvingCheckOrder xmlns=\"http://mesateapi.com/\">\r\n");
            soap.Append($"      <orderNumber>{txtOrder.Text}</orderNumber>\r\n");
            soap.Append($"      <itemCode>{txtItem.Text}</itemCode>\r\n");
            soap.Append("    </LaserCarvingCheckOrder>\r\n");
            soap.Append("  </soap12:Body>\r\n");
            soap.Append("</soap12:Envelope>");

            Uri uri = new Uri(txtUrl1.Text);
            string a = ""; ;
            try
            {
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.ContentType = "application/soap+xml; charset=utf-8";
                webRequest.Method = "POST";
                using (Stream requestStream = webRequest.GetRequestStream())
                {
                    byte[] paramBytes = Encoding.UTF8.GetBytes(soap.ToString());
                    requestStream.Write(paramBytes, 0, paramBytes.Length);
                }
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                {
                    a += myStreamReader.ReadToEnd();
                }
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(a);
                XmlElement xmlElement = doc.DocumentElement;
                var rtn = doc.GetElementsByTagName("LaserCarvingCheckOrderResult")?[0].InnerText;
                var b = rtn.Split(';');
                if (b?[0] == "OK")
                {
                    MessageBox.Show("验证OK");
                    this.BeginInvoke(new Action(()=>
                    {
                        if (txtPrint.Text == string.Empty && bsUnPrint.List.Count == 0) 
                        {
                            Requst_SN();
                        }
                        if (IsEmpty)
                        {
                            btnSet_Click(null,null);
                        }
                    }));
                }
                else
                    MessageBox.Show($"验证失败:{b?[1]}");

            }
            catch (Exception err)
            {

            }
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var settings = configFile.AppSettings.Settings;
            if (settings["action1"] == null)
            {
                settings.Add("action1", txtUrl1.Text);
            }
            else
            {
                settings["action1"].Value = txtUrl1.Text;
            }
            if (settings["sport"] == null)
            {
                settings.Add("sport", numPort.Text);
            }
            else
            {
                settings["sport"].Value = numPort.Text;
            }
            if (settings["auto"] == null)
            {
                settings.Add("auto", cbAuto.Checked.ToString());
            }
            else
            {
                settings["auto"].Value = cbAuto.Checked.ToString();
            }
            configFile.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
        }

        private void BsUnPrint_ListChanged(object sender, ListChangedEventArgs e)
        {
            if (bsUnPrint.List.Count < 3 && e.ListChangedType == ListChangedType.ItemDeleted||(bsUnPrint.List.Count == 0)) 
            {
                Requst_SN();
            }
        }

        private void Requst_SN()
        {
            StringBuilder soap = new StringBuilder();
            soap.Append("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\r\n");
            soap.Append("  <soap12:Body>\r\n");
            soap.Append("        <LaserCarvingGetOrderSN xmlns = \"http://mesateapi.com/\">\r\n");
            soap.Append($"      <orderNumber>{txtOrder.Text}</orderNumber>\r\n");
            soap.Append($"      <itemCode>{txtItem.Text}</itemCode>\r\n");
            soap.Append($"      <qty>{numCount.Text}</qty>\r\n");
            soap.Append("    </LaserCarvingGetOrderSN>\r\n");
            soap.Append("  </soap12:Body>\r\n");
            soap.Append("</soap12:Envelope>");

            Uri uri = new Uri(txtUrl1.Text);
            string a = ""; ;
            try
            {
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.ContentType = "application/soap+xml; charset=utf-8";
                webRequest.Method = "POST";
                using (Stream requestStream = webRequest.GetRequestStream())
                {
                    byte[] paramBytes = Encoding.UTF8.GetBytes(soap.ToString());
                    requestStream.Write(paramBytes, 0, paramBytes.Length);
                }
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                {
                    a += myStreamReader.ReadToEnd();
                }
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(a);
                XmlElement xmlElement = doc.DocumentElement;
                var rtn = doc.GetElementsByTagName("LaserCarvingGetOrderSNResult")?[0].InnerText;
                string[] b = rtn.Split(';');
                if (b?[0] == "OK")
                {
                    var c = b.ToList();
                    unPrintSN.AddRange(c.Last().Split(','));
                    this.Invoke(new Action(() => {
                        bsUnPrint.ResetBindings(false);
                        if (txtPrint.Text == "")
                        {
                            txtPrint.Text = bsUnPrint.List?[0].ToString();
                            bsUnPrint.Remove(txtPrint.Text);
                        }
                    }));
                }
                else
                    MessageBox.Show($"获取失败:{b?[1]}");

            }
            catch (Exception err)
            {

            }
        }

        private void btnSet_Click(object sender, EventArgs e)
        {
            if (lstClient.Items.Count == 0)
            {
                MessageBox.Show("没有连接的客户端");
                return;
            }

            SocketConnection socketcon;
            this.Invoke(new Action(() =>
            {
                bsUnPrint.Remove(txtPrint.Text);
                bsPrinted.Add(txtPrint.Text);
                //unPrintSN.Remove(txtPrint.Text);
            }));
            bAutoSetFlag = false;
            byte[] cmd = PmjInstruct.JointCmd(PmjCmd.SetContent, txtPrint.Text);
            foreach (string select in lstClient.Items)
            {
                if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(select, out socketcon) &&
                socketcon.ConnectSocket.Connected)
                {
                    socketcon.Send(cmd);
                }
                else
                {
                    lstClient.Items.Remove(select);
                    SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(select, out socketcon);
                }
            }
            bAutoSetFlag = true;
        }

        private void btnUpLoad_Click(object sender, EventArgs e)
        {
            string json = string.Join(",", printedSN.Select(m => "\"" + m + "\"" + ":\"" + "OK" + "\""));
            json = "{" + json + "}";
            StringBuilder soap = new StringBuilder();
            soap.Append("<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\r\n");
            soap.Append("  <soap12:Body>\r\n");
            soap.Append("        <LaserCarvingUpdateSNStatus xmlns = \"http://mesateapi.com/\" >\r\n");
            soap.Append($"      <orderNumber>{txtOrder.Text}</orderNumber>\r\n");
            soap.Append($"      <itemCode>{txtItem.Text}</itemCode>\r\n");
            soap.Append($"      <json>{json}</json>\r\n");
            soap.Append("    </LaserCarvingUpdateSNStatus>\r\n");
            soap.Append("  </soap12:Body>\r\n");
            soap.Append("</soap12:Envelope>");

            Uri uri = new Uri(txtUrl1.Text);
            string a = ""; ;
            try
            {
                WebRequest webRequest = WebRequest.Create(uri);
                webRequest.ContentType = "application/soap+xml; charset=utf-8";
                webRequest.Method = "POST";
                using (Stream requestStream = webRequest.GetRequestStream())
                {
                    byte[] paramBytes = Encoding.UTF8.GetBytes(soap.ToString());
                    requestStream.Write(paramBytes, 0, paramBytes.Length);
                }
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                {
                    a += myStreamReader.ReadToEnd();
                }
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(a);
                XmlElement xmlElement = doc.DocumentElement;
                var rtn = doc.GetElementsByTagName("LaserCarvingUpdateSNStatusResult")?[0].InnerText;
                string[] b = rtn.Split(';');
                if (b?[0] == "OK")
                {
                    printedSN.Clear();
                }
                else
                    MessageBox.Show($"上传失败:{b?[1]}");

            }
            catch (Exception err)
            {

            }
        }

        private void btnGetSN_Click(object sender, EventArgs e)
        {
            Requst_SN();
        }

        private void btnSetPrintCount_Click(object sender, EventArgs e)
        {
            if (numCount2.Text == "")
            {
                MessageBox.Show("请输入设定次数");
                return;
            }
            if (lstClient.Items.Count == 0)
            {
                MessageBox.Show("没有连接的客户端");
                return;
            }
            SocketConnection socketcon;

            foreach (string item in lstClient.Items)
            {
                SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(item, out socketcon);
                SetCount(socketcon, numCount2.Value.ToString());
            }
        }

        private void SetCount(SocketConnection socketcon, string count)
        {
            var bs = PMJ.Instruct.PmjInstruct.JointCmd(PmjCmd.SetCount, count);

            if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(socketcon.Identity, out socketcon) &&
            socketcon.ConnectSocket.Connected)
            {
                socketcon.Send(bs);
            }
            else
            {
                lstClient.Items.Remove(socketcon);
                SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(socketcon.Identity, out socketcon);
            }

        }

        private void SetContent(SocketConnection socketcon, string content)
        {
            var bs = PMJ.Instruct.PmjInstruct.JointCmd(PmjCmd.SetContent, content);

            if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(socketcon.Identity, out socketcon) &&
            socketcon.ConnectSocket.Connected)
            {
                socketcon.Send(bs);
            }
            else
            {
                lstClient.Items.Remove(socketcon);
                SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(socketcon.Identity, out socketcon);
            }

        }

        private void btnClearCache_Click(object sender, EventArgs e)
        {
            var bs = PMJ.Instruct.PmjInstruct.JointCmd(PmjCmd.ClearCache ,null);
            if (lstClient.Items.Count == 0)
            {
                MessageBox.Show("没有连接的客户端");
                return;
            }
      
            SocketConnection socketcon;
            foreach (string select in lstClient.Items)
            {
                if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(select, out socketcon) &&
                socketcon.ConnectSocket.Connected)
                {
                    socketcon.Send(bs);
                }
                else
                {
                    lstClient.Items.Remove(select);
                    SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(select, out socketcon);
                }
            }
        }

        private void btnDisconnect_Click(object sender, EventArgs e)
        {
            var bs = PMJ.Instruct.PmjInstruct.JointCmd(PmjCmd.DisConnect, null);
            if (lstClient.Items.Count == 0)
            {
                MessageBox.Show("没有连接的客户端");
                return;
            }

            SocketConnection socketcon;
            foreach (string select in lstClient.Items)
            {
                if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(select, out socketcon) &&
                socketcon.ConnectSocket.Connected)
                {
                    socketcon.Send(bs);
                }
                else
                {
                    lstClient.Items.Remove(select);
                    SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(select, out socketcon);
                }
            }
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            var bs = PMJ.Instruct.PmjInstruct.JointCmd(PmjCmd.Continue, null);
            if (lstClient.Items.Count == 0)
            {
                MessageBox.Show("没有连接的客户端");
                return;
            }

            SocketConnection socketcon;
            foreach (string select in lstClient.Items)
            {
                if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(select, out socketcon) &&
                socketcon.ConnectSocket.Connected)
                {
                    socketcon.Send(bs);
                }
                else
                {
                    lstClient.Items.Remove(select);
                    SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(select, out socketcon);
                }
            }
        }

        private void btnCancelPrint_Click(object sender, EventArgs e)
        {
            var bs = PMJ.Instruct.PmjInstruct.JointCmd(PmjCmd.CancelPrint, null);
            if (lstClient.Items.Count == 0)
            {
                MessageBox.Show("没有连接的客户端");
                return;
            }

            SocketConnection socketcon;
            foreach (string select in lstClient.Items)
            {
                if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(select, out socketcon) &&
                socketcon.ConnectSocket.Connected)
                {
                    socketcon.Send(bs);
                }
                else
                {
                    lstClient.Items.Remove(select);
                    SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(select, out socketcon);
                }
            }
        }

        private void txtPrint_TextChanged(object sender, EventArgs e)
        {
            //if (txtPrint.Text == string.Empty && bsUnPrint.List?.Count != 0)
            //{
            //    txtPrint.Text = bsUnPrint.List?[0].ToString();
            //    List<string> selectList = new List<string>();
            //    foreach (var selectedItem in lstClient.Items)
            //    {
            //        selectList.Add(selectedItem.ToString());
            //    }
            //    SocketConnection socketcon;
            //    byte[] cmd = PmjInstruct.JointCmd(PmjCmd.SetContent, txtPrint.Text);
            //    foreach (var socketcon in selectList)
            //    {
            //        if (SocketConnectionServerDispatcher.DicSockectConnection.TryGetValue(socketcon, out socketcon) &&
            //        socketcon.ConnectSocket.Connected)
            //        {
            //            socketcon.Send(cmd);
            //        }
            //        else
            //        {
            //            lstClient.Items.Remove(socketcon);
            //            SocketConnectionServerDispatcher.DicSockectConnection.TryRemove(socketcon, out socketcon);
            //        }
            //    }
            //}
        }
    }
}
