﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using PMJ.Socket.Model;
using PMJ.Socket.Util;

namespace PMJ.Socket
{
    /// <summary>
    /// 智能家居
    /// </summary>
    public class SocketConnection : ISocketConnection
    {
        #region Static Member        
        /// <summary>
        /// 各种回调的配置
        /// </summary>
        protected static SocketConnectConfig Sktconfig;
        /// <summary>
        /// 每一个会话的缓存大小
        /// </summary>
        protected static int BufferSize;
        /// <summary>
        /// 设置编码格式
        /// </summary>
        protected static readonly Encoding Encoder = Encoding.UTF8;
        #endregion

        #region Properity
        /// <summary>
        /// 当前客户端socket连接
        /// </summary>
        public System.Net.Sockets.Socket ConnectSocket { get; }

        /// <summary>
        /// 当前socket会话次数
        /// </summary>
        private int TotalSession { set; get; }

        /// <summary>
        /// 子连接数
        /// </summary>
        public int SubConnection { get; set; } = 0;

        /// <summary>
        /// 最近一次会话时间
        /// </summary>
        public DateTime LastSessionTime { private set; get; }

        /// <summary>
        /// 粘包情况下，上次最末尾未处理的包 -----修改为，直接舍弃
        /// </summary>
        //private byte[] _prePartialReceiveByte;

        /// <summary>
        /// 当前连接的客户端标识,最大长度16位字符串
        /// </summary>
        public string Identity { get; set; } = "";

        /// <summary>
        /// socket连接类型
        /// </summary>
        public SocketConnectType SocketConnectType { set; get; }
        #endregion

        #region construct

        public SocketConnection(System.Net.Sockets.Socket connect)
        {
            ConnectSocket = connect;
        }

        public virtual void Disconnect(Action<string> disConnectCallback = null)
        {
            //string localIpEndPort = ConnectSocket.LocalEndPoint.ToString();
            if (ConnectSocket != null && Identity != null && ConnectSocket.Connected)
            {
                ConnectSocket.BeginDisconnect(false, ar =>
                {
                    disConnectCallback?.Invoke(Identity);
                    //LogMsg(localIpEndPort,remoteIpEndPort,$"disconnect socket remote {remoteIpEndPort} local {localIpEndPort}");
                }, null);
            }

        }
        #endregion

        #region Receive
        /// <summary>
        /// 接收数据
        /// </summary>
        public virtual void ReceiveData(Action<byte[], SocketConnection> receiveCallback = null)
        {
            try
            {
                var srb = new SocketReceiveBack() { Buffer = new byte[BufferSize], ReceiveCallback = receiveCallback };
                ConnectSocket.BeginReceive(srb.Buffer, 0, BufferSize, SocketFlags.None, ReceiveCallback, srb);
            }
            catch (SocketException sktex)
            {
                SocketException(ConnectSocket, sktex);
            }
            catch (Exception ex)
            {
                Exception(ex);
            }
        }

        public virtual void ReceiveCallback(IAsyncResult ar)
        {
            if (TotalSession >= 10000) TotalSession = 0;
            TotalSession++;
            System.Net.Sockets.Socket handler = ConnectSocket;
            var srb = ar.AsyncState as SocketReceiveBack;
            try
            {
                if (handler != null && !handler.Connected) //当近端主动Disconnect时会触发
                {
                    Sktconfig?.DisConnectCallback?.Invoke(this);
                    //LogMsg(handler.LocalEndPoint.ToString(), handler.RemoteEndPoint.ToString(), $"disconnect remote {handler.RemoteEndPoint}\r\n");
                    return;
                }
                //最大是设置的msgbuffer的长度，如果发送的数据超过了buffer的长度就分批次接收
                //这里在客户端强制关闭也会进去这个回调，但是执行到EndReceive时会出错
                SocketError se;
                var rEnd = handler?.EndReceive(ar, out se);

                if (rEnd > 0 && srb != null)
                {
                    var receiveBytes = srb.Buffer.Take(rEnd.Value).ToArray();
                    handler.BeginReceive(srb.Buffer, 0, BufferSize, 0, ReceiveCallback, srb);
                    #region 解析每一个数据包
                    //LogMsg(handler.LocalEndPoint.ToString(), handler.RemoteEndPoint.ToString(), $"current session from remote {handler.RemoteEndPoint} receive data [{ BitConverter.ToString(receiveByte)}]\r\n");
                    var bytepackage = GetPackage(receiveBytes);
                    int queeCount = bytepackage.Count;
                    for (int i = 0; i < queeCount; i++)
                    {
                        //下面是正常业务逻辑处理
                        var bytemsg = bytepackage.Dequeue();
                        if (bytemsg.Length == 0) continue;
                        //LogMsg(handler.LocalEndPoint.ToString(), handler.RemoteEndPoint.ToString(), $"from {handler.RemoteEndPoint} receive [{ BitConverter.ToString(bytemsg)}]\r\n");

                        if (srb.MsgRecevieCallBack != null)
                        {
                            srb.MsgRecevieCallBack(bytemsg, this);
                        }
                        else
                        {
                            Sktconfig?.ReceiveCallback?.Invoke(bytemsg, this);
                        }
                    }

                    #endregion
                }
                else //远端主动Disconnect时会到达这里
                {
                    if (handler != null)
                    {
                        Sktconfig?.DisConnectCallback?.Invoke(this);
                        //LogMsg(handler.LocalEndPoint.ToString(), handler.RemoteEndPoint.ToString(), $"remote {handler.RemoteEndPoint} has close\r\n");
                    }
                }
                LastSessionTime = DateTime.Now;
            }
            catch (SocketException sktex)
            {
                SocketException(handler, sktex);
            }
            catch (Exception ex)
            {
                Exception(ex);
            }
        }

        private Queue<byte[]> GetPackage(byte[] receiveByte)
        {
            var queePackage = new Queue<byte[]>();
            int msgLength;

            if (!receiveByte.Any(b => b == 0x0A)||receiveByte.Length < 6)//当前包也是同一个消息中的一部分
            {
                queePackage.Enqueue(receiveByte);
                return queePackage;
            }

            var pkg = Encoding.ASCII.GetString(receiveByte);
            msgLength = pkg.IndexOf("\r\n");
            if (msgLength < 0)
            {
                queePackage.Enqueue(receiveByte);
                return queePackage;
            }
            msgLength += 2;
            while (msgLength <= receiveByte.Length)
            {
                var takeBytes = receiveByte.Take(msgLength).ToArray();
                queePackage.Enqueue(takeBytes.ToArray());

                receiveByte = receiveByte.Skip(msgLength).ToArray();
                pkg = Encoding.ASCII.GetString(receiveByte);
                msgLength = pkg.IndexOf("\r\n");
                if (msgLength < 0 && receiveByte.Length != 0) 
                {
                    queePackage.Enqueue(receiveByte);
                    break;
                }
                msgLength += 2;

                //if (receiveByte.Any(b => b == 0x0A))
                //{
                //    byte[] byts = new byte[4];
                //    Array.Copy(receiveByte, 0, byts, 0, 4);
                //    msgLength = BitConverter.ToInt32(byts, 0);
                //}
                //else
                //{
                //    msgLength = int.MaxValue;
                //    break;
                //}
            }
            return queePackage;
        }
        #endregion

        #region Send
        /// <summary>
        /// 发送字节数组，针对websocket协议本身就定义了前2，4，8字节为数据长度，所以不用加上自定义的数据帧长度了
        /// </summary>
        /// <param name="byteData"></param>
        /// <param name="sendCallBack">发送数据当前回调</param>
        public virtual void Send(byte[] byteData, Action<SocketConnection> sendCallBack = null)
        {
            try
            {
#if DEBUG
                //LogMsg(ConnectSocket.LocalEndPoint.ToString(), ConnectSocket.RemoteEndPoint.ToString(), $"to {ConnectSocket.RemoteEndPoint} data [{BitConverter.ToString(byteData)}]\r\n");
#endif
                Sktconfig?.BeforeSend?.Invoke(byteData, this);//发送之前的操作
                ConnectSocket.BeginSend(byteData, 0, byteData.Length, 0, SendCallback, sendCallBack);
            }
            catch (SocketException sktex)
            {
                SocketException(ConnectSocket, sktex);
            }
            catch (Exception ex)
            {
                Exception(ex);
            }
        }

        /// <summary>
        /// 发送完毕的回调
        /// </summary>
        /// <param name="ar"></param>
        public virtual void SendCallback(IAsyncResult ar)
        {
            System.Net.Sockets.Socket handler = ConnectSocket;
            var sendCallBack = ar.AsyncState as Action<SocketConnection>;
            try
            {
                handler.EndSend(ar); //这里写发送完毕的逻辑
                sendCallBack?.Invoke(this);
            }
            catch (SocketException sktex)
            {
                SocketException(handler, sktex);
            }
            catch (Exception ex)
            {
                Exception(ex);
            }
        }
        #endregion

        #region ShowMsg | Log | Exception
        /// <summary>
        /// 所有记录的信息都可以显示出来
        /// </summary>
        /// <param name="content">显示内容</param>
        [Conditional("DEBUG")]
        public static void ShowMsg(string content)
        {
            Sktconfig?.ShowMsg?.Invoke(content);
        }

        /// <summary>
        /// 记录信息，记录的可以是消息、异常等信息
        /// </summary>
        /// <param name="localIpEndPoint">本地Ip端口</param>
        /// <param name="remoteIpEndPoint">远端Ip端口</param>
        /// <param name="content"></param>
        [Conditional("DEBUG")]
        protected static void LogMsg(string localIpEndPoint, string remoteIpEndPoint, string content)
        {
            ShowMsg(DateTime.Now.ToString("yy/MM/dd HH:mm:ss.fffffff") + " " + content);
            //Log.LogModel(localIpEndPoint, remoteIpEndPoint, content);
        }

        /// <summary>
        /// socket异常处理
        /// </summary>
        /// <param name="handler">socket对象</param>
        /// <param name="ex">异常对象</param>
        protected static void SocketException(System.Net.Sockets.Socket handler, SocketException ex)
        {
            ShowMsg(ex.Message + "\r\n" + ex.StackTrace);
            Sktconfig?.SocketConnectExceptionCallback?.Invoke(ex);
            //Log.LogModel(handler.Connected
            //    ? new LogEntity<object>()
            //    {
            //        LogType = LogType.SocketException,
            //        LocalIpEndPoint = handler.LocalEndPoint.ToString(),
            //        RemoteIpEndPoint = handler.RemoteEndPoint.ToString(),
            //        LogContent = ex
            //    }
            //    : new LogEntity<object>()
            //    {
            //        LogType = LogType.SocketException,
            //        LocalIpEndPoint = handler.LocalEndPoint.ToString(),
            //        LogContent = ex
            //    });
        }

        /// <summary>
        /// 异常处理
        /// </summary>
        /// <param name="ex">异常对象</param>
        protected static void Exception(Exception ex)
        {
            ShowMsg(ex.Message + "\r\n" + ex.StackTrace);
            //Log.LogModel(new LogEntity<object>() { LogType = LogType.Exception, LogContent = ex });
        }
        #endregion

    }
}
