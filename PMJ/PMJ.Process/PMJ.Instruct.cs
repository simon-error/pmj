﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMJ.Instruct
{
    public class PmjInstruct
    {
        static byte[] startBytes = { 0x1B, 0x53 };

        static byte[] endBytes = { 0x0d, 0x0a };

        public static byte[] JointCmd(PmjCmd cmd,string value)
        {
            
            List<byte> instruct = new List<byte>();
            instruct.AddRange(startBytes);
            switch (cmd)
            {
                case PmjCmd.CancelPrint:
                    instruct.AddRange(Encoding.Default.GetBytes("#c"));
                    break;
                case PmjCmd.Continue:
                    instruct.AddRange(Encoding.Default.GetBytes("#d"));
                    break;
                case PmjCmd.ClearCache:
                    instruct.AddRange(Encoding.Default.GetBytes("77"));
                    break;
                case PmjCmd.DisConnect:
                    instruct.AddRange(Encoding.Default.GetBytes("CC"));
                    break;
                case PmjCmd.SetContent:
                    if (value == string.Empty)
                    {
                        instruct.Clear();
                        return instruct.ToArray();
                    }
                    instruct.AddRange(Encoding.Default.GetBytes("11"));
                    instruct.AddRange(Encoding.Default.GetBytes(value));
                    break;
                case PmjCmd.SetCount:
                    if (value== string.Empty)
                    {
                        instruct.Clear();
                        return instruct.ToArray();
                    }
                    instruct.AddRange(new byte[] { 0xFF,0xFF});

                    int count =1; 
                    int.TryParse(value.ToString(), out count);
                    byte[] bytes = BitConverter.GetBytes(count);
                    if (!BitConverter.IsLittleEndian)
                        Array.Reverse(bytes); //reverse it so we get little endian.
                    instruct.AddRange(bytes);
                    instruct.RemoveAt(instruct.Count() - 1);
                    break;
                default:
                    break;
            }
            instruct.Add((byte)CheckSum(instruct));
            instruct.AddRange(endBytes);
            return instruct.ToArray();
        }



        private static int CheckSum(List<byte> instruct)
        {
            int a=0;
            for (int i = 0; i < instruct.Count; i++)
            {
                a += instruct[i];
                a &= 0xff;
            }
            return a;
        }
    }

    public enum PmjCmd
    {
        CancelPrint,
        ClearCache,
        DisConnect,
        SetContent,
        SetCount,
        Continue
    }
}
